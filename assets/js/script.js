// alert("w3rKiNg bh4 t0h j3333m?? pibertdi!!");

let perHour = document.querySelector("#bawatOras");

let perMinute = document.querySelector("#bawatMinuto");

let perSecond = document.querySelector("#bawatSegundo");

let currentDate = document.querySelector("#petsa");

let changeTxtFormatBtn = document.querySelector("#palitanAngFormat");

let clockDiv = document.querySelector("#orasan");

let digitalFont = document.querySelector("#palitanAngFont1");

let orbitronFont = document.querySelector("#palitanAngFont2");

let textColors = document.querySelectorAll(".mgaKulay");

let clockSpan = document.querySelectorAll(".spans");

// console.log(clockDiv.innerText);




let is24Hour = false;


let clock = () => {

	let fullTime = new Date();
	let hours = fullTime.getHours();
	let minutes = fullTime.getMinutes();
	let seconds = fullTime.getSeconds();
	let date = fullTime.toDateString();
	

	if (hours < 10) {
		hours = "0" + hours;
	}
	if (minutes < 10) {
		minutes = "0" + minutes;
	}
	if (seconds < 10) {
		seconds = "0" + seconds;
	}

	if (is24Hour) {
        perHour.innerHTML = hours; 
        perMinute.innerHTML = minutes;
        perSecond.innerHTML = seconds;
    }
    else {
        suffix = hours >= 12 ? "PM" : "AM",
        hours12 = hours % 12;

        perHour.innerHTML = hours12; 
        perMinute.innerHTML = minutes;
        perSecond.innerHTML = seconds + " " + suffix;
    }


	currentDate.innerHTML = date;

};


setInterval(clock, 1000);




let changeFormat = () => {
    is24Hour = !is24Hour;
};



changeTxtFormatBtn.addEventListener("click", changeFormat);

textColors.forEach( (c) => {
	c.addEventListener("click", () => {
		currentDate.style.color = c.innerHTML;
		clockDiv.style.boxShadow = "0px 0px 10px " + c.innerHTML;

		clockSpan.forEach( (s) => {
			s.style.color = c.innerHTML;
		}); 
	});
});


digitalFont.addEventListener("click", () => {
	clockDiv.style.fontFamily = "Digital";
	currentDate.style.fontFamily = "Digital";
	clockDiv.style.fontSize = "200px";

});



orbitronFont.addEventListener("click", () => {
	clockDiv.style.fontFamily = 'Orbitron';
	currentDate.style.fontFamily = 'Orbitron';
	clockDiv.style.fontSize = "140px";
	currentDate.style.fontSize = '100px';
});


let updateBackground = () => {
	let hr = new Date();
	let everyHr = hr.getHours();
    body = document.body
    bstyle = body.style       
  if (everyHr <= 5) {
    bstyle.backgroundImage = "url('assets/images/else.jpg')";
    bstyle.backgroundRepeat = "no-repeat";
  	bstyle.backgroundAttachment = "fixed";  
  	bstyle.backgroundSize = "cover";
  } else if (everyHr <= 6) {
    bstyle.backgroundImage = "url('assets/images/6am.png')";
    bstyle.backgroundRepeat = "no-repeat";
  	bstyle.backgroundAttachment = "fixed";  
  	bstyle.backgroundSize = "cover";
  }else if (everyHr <= 9) {
    bstyle.backgroundImage = "url('assets/images/9am.png')";
    bstyle.backgroundRepeat = "no-repeat";
  	bstyle.backgroundAttachment = "fixed";  
  	bstyle.backgroundSize = "cover";
  } else if (everyHr <= 12) {
    bstyle.backgroundImage = "url('assets/images/12nn.png')";
    bstyle.backgroundRepeat = "no-repeat";
  	bstyle.backgroundAttachment = "fixed";  
  	bstyle.backgroundSize = "cover"
  } else if (everyHr <= 16) {
    bstyle.backgroundImage = "url('assets/images/4pm.png')";
    bstyle.backgroundRepeat = "no-repeat";
  	bstyle.backgroundAttachment = "fixed";  
  	bstyle.backgroundSize = "cover";
  } else if (everyHr <= 17) {
    bstyle.backgroundImage = "url('assets/images/5pm.png')";
    bstyle.backgroundRepeat = "no-repeat";
  	bstyle.backgroundAttachment = "fixed";  
  	bstyle.backgroundSize = "cover";
  } else if (everyHr <= 18) {
    bstyle.backgroundImage = "url('assets/images/6pm.png')";
    bstyle.backgroundRepeat = "no-repeat";
  	bstyle.backgroundAttachment = "fixed";  
  	bstyle.backgroundSize = "cover";
  } else if (everyHr <= 19) {
    bstyle.backgroundImage = "url('assets/images/7pm.png')";
    bstyle.backgroundRepeat = "no-repeat";
  	bstyle.backgroundAttachment = "fixed";  
  	bstyle.backgroundSize = "cover";
  } else {
    bstyle.backgroundImage = "url('assets/images/else.jpg')";
    bstyle.backgroundRepeat = "no-repeat";
  	bstyle.backgroundAttachment = "fixed";  
  	bstyle.backgroundSize = "cover";
  } 

  // else {
  //   bstyle.backgroundImage = "url('assets/images/9pm.png')";
  //   bstyle.backgroundRepeat = "no-repeat";
  // 	bstyle.backgroundAttachment = "fixed";  
  // 	bstyle.backgroundSize = "cover"
  // } 
}
setInterval(updateBackground, 1000 * 60);

updateBackground();